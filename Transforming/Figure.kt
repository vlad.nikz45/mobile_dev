
abstract class Figure(var x: Int, var y: Int, var name: String) {

    abstract fun area(): Float


}