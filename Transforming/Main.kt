fun main() {
    val rect = Rect(0, 0, "Прямоугольник", 8, 2)
    val square = Square(2, -2, "Квадрат", 4)
    val circle = Circle(1, 3, "Круг", 6)

    // Операции с прямоугольником
    println(rect)
    println("Переместить в точку (6, 4): ")
    rect.move(6, 4)
    println(rect)
    println("Повернуть вокруг точки (7, -2) по часовой стрелке: ")
    rect.rotate(RotateDirection.Clockwise, 7, -2)
    println(rect)
    println("Изменить размер в 2 раза: ")
    rect.resize(2)
    println(rect)
    println("Повернуть вокруг точки (7, -2) против часовой стрелки: ")
    rect.rotate(RotateDirection.CounterClockwise, 7, -2)
    println(rect)
    println()

    // Операции с квадратом
    println(square)
    println("Переместить в точку (5, 3): ")
    square.move(5, 3)
    println(square)
    println("Повернуть вокруг точки (6, -4) по часовой стрелке: ")
    square.rotate(RotateDirection.Clockwise, 6, -4)
    println(square)
    println("Изменить размер в 2 раза: ")
    square.resize(2)
    println(square)
    println("Повернуть вокруг точки (6, -4) против часовой стрелки: ")
    square.rotate(RotateDirection.CounterClockwise, 6, -4)
    println(square)

    println()

    // Операции с кругом
    println(circle)
    println("Переместить в точку (3, 7): ")
    circle.move(3, 7)
    println(circle)
    println("Повернуть вокруг точки (2, 5) по часовой стрелке: ")
    circle.rotate(RotateDirection.Clockwise, 2, 5)
    println(circle)
    println("Изменить размер в 2 раза: ")
    circle.resize(2)
    println(circle)
    println("Повернуть вокруг точки (2, 5) против часовой стрелки: ")
    circle.rotate(RotateDirection.CounterClockwise, 2, 5)
    println(circle)
    println()
}
