class Rect(x: Int, y: Int, name: String, var width: Int, var height: Int) : Movable, Figure(x, y, name), Transforming {

    constructor(rect: Rect) : this(rect.x, rect.y, rect.name, rect.width, rect.height)


    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun resize(zoom: Int) {
        width *= zoom
        height *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        height = width.also { width = height }
        val y1 = y
        x = if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            centerX + (y1 - centerY)
        } else {
            y = centerY + (x - centerX)
            centerX - (y1 - centerY)
        }
    }



    override fun area(): Float {
        return (width * height).toFloat()
    }

    override fun toString(): String {
        return "$name находится в точке ($x,$y), его длина = $width, ширина = $height"
    }
}