
class Square(x: Int, y: Int, name: String, var a: Int) : Figure(x, y, name), Transforming, Movable {

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun resize(zoom: Int) {
        a *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val y1 = y
        x = if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            centerX + (y1 - centerY)
        } else {
            y = centerY + (x - centerX)
            centerX - (y1 - centerY)
        }
    }

    override fun area(): Float {
        return (a * a).toFloat()
    }

    override fun toString(): String {
        return "$name находится в точке ($x,$y), его сторона = $a"
    }
}